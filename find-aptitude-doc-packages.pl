#!/usr/bin/perl

use strict;
use warnings;
use 5.010;

my $mirror = $ARGV[0] || 'http://httpredir.debian.org/debian/';
my $pkg;

while (<STDIN>) {
    # Clear state
    if (/^$/) {
	undef $pkg;
	next;
    }

    # Package found
    if (/^Package: aptitude-doc-/) {
	$pkg = 1;
    }

    if ($pkg and /^Filename: (.*)$/) {
	say "$mirror/$1";
	next;
    }
}
